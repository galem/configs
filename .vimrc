syntax on
filetype on
filetype plugin indent on

" Remap leader key
:let mapleader = ","

" Insert 4 spaces when tab is pressed, and 4 spaces for indention
set tabstop=4 shiftwidth=4 expandtab

" use indentation of previous line, and take into account current coding
" syntax
set autoindent smartindent cindent

set number 
set showcmd
set cursorline
set wildmenu
set showmatch

" No swap files
set noswapfile 

" Searching bits. Case insensitive seach. Smartcase will do a case 
" senstive search if using caps
set incsearch
set ignorecase
set smartcase

filetype indent on

" set UTF-8 encoding
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

" disable vi compatibility (emulation of old bugs)
set nocompatible

" automatically insert a closing parenthesis when typing an opening parenthesis
"inoremap ( ()<Esc>i

" Type one brack and it creates another and put it on newline and put in
" insert mode ready for coding
"inoremap { {<CR><BS>}<Esc>ko

" intelligent comments
set comments=sl:/*,mb:\ *,elx:\ */

" Use tab for auto completetion
let g:SuperTabDefaultCompletionType = "<C-X><C-O>"

"Disable arrow keys. Practice makes perfect!
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

"Automatic toggling between relative and regular line number
:set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Get to normal mode with jj instead 
:imap jj <Esc>

" Navigate Panes 
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Statusline
set statusline =
" Buffer number
set statusline +=[%n]
" File description
set statusline +=%f\ %h%m%r%w
" Filetype
set statusline +=%y                                                  
" Name of the current function (needs taglist.vim)
set statusline +=\ [Fun(%{Tlist_Get_Tagname_By_Line()})]
" Name of the current branch (needs fugitive.vim)
set statusline +=\ %{fugitive#statusline()}
" Date of the last time the file was saved
set statusline +=\ %{strftime(\"[%d/%m/%y\ %T]\",getftime(expand(\"%:p\")))} 
" Total number of lines in the file
set statusline +=%=%-10L
" Line, column and percentage
set statusline +=%=%-14.(%l,%c%V%)\ %P

"
"########### Plugins Specific Stuffs #############
"
"     -- NerdTree --
"Toggle NERDtree to Ctrl+N
nmap <C-N> :NERDTreeToggle<CR>
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

"     -- Cscope --
nnoremap <leader>fa :call CscopeFindInteractive(expand('<cword>'))<CR>
nnoremap <leader>l :call ToggleLocationList()<CR>

"     -- CtrlP --
" Fix CtrlP opening buffers in split and invoke ctrlp with ctrl+p
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_reuse_window = 'startify'

"      -- SearchTasks --
" Keywords for searchtask to look for
let g:searchtasks_list=["TODO", "FIXME", "XXX"]

"       -- TagBar --
" Map the tagbar to F8
nmap <F8> :TagbarToggle<CR>

"       -- Vim Airline --
let g:airline_powerline_fonts = 1

"        -- VimPlug --
" Install vim-plug if it is not installed
if empty(glob('~/.vim/autoload/plug.vim'))
          silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
              \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
            autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plugins to be installed 
call plug#begin('~/.vim/plugged')

Plug 'kien/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-easytags'
Plug 'brookhong/cscope.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'SirVer/ultisnips'
Plug 'jeetsukumaran/vim-buffergator'
Plug 'gilsondev/searchtasks.vim'
Plug 'majutsushi/tagbar'
Plug 'simeji/winresizer'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'powerline/powerline'
Plug 'vim-scripts/taglist.vim'
Plug 'tpope/vim-fugitive'
Plug 'hashivim/vim-vagrant'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'zivyangll/git-blame.vim'

" Themes to be installed
Plug 'vim-scripts/dante.vim'
Plug 'gkjgh/cobalt'

call plug#end()

" Set Colorscheme
colorscheme cobalt

