#!/bin/bash

destination=""
files=""

while getopts ":d:f:" opt; do
    case $opt in
        d)
            destination=$OPTARG
            ;;
        f)
            files=$OPTARG
            ;;
        \?)
            echo "Invalid arg: -$OPTARG"
            exit 1
            ;;
        :)
            echo "Option $OPTARG requires an argument."
            exit 1
    esac
done

update_vim() {
    if [ "$destination" == "local" ]; then
        echo "[+] Updating local copy of vimrc"
        cp .vimrc $HOME/
    else
        echo "[+] Updating remote copy of vimrc"
        cp $HOME/.vimrc .
    fi
}

update_tmux() {
    if [ "$destination" == "local" ]; then
        echo "[+] Updating local copy of tmux"
        pushd $HOME
        if [ ! -d ".tmux" ]; then
            git clone https://github.com/gpakosz/.tmux.git
            ln -s -f .tmux/.tmux.conf
            cp .tmux/.tmux.conf.local .
        fi
        popd
        cp .tmux.conf.local $HOME/
    else
        echo "[+] Updating remote copy of tmux"
        cp $HOME/.tmux.conf.local .
    fi
}

update_git() {
    if [ "$destination" == "local" ]; then
        echo "[+] Updating local copy of gitconfig"
        cp .gitconfig $HOME/
    else
        echo "[+] Updating remote copy of gitconfig"
        cp $HOME/.gitconfig .
    fi
}

if [ "$files" == "all" ]; then
        update_vim
        update_tmux
        update_git
elif [ "$files" == "vim" ]; then
        update_vim
elif [ "$files" == "tmux" ]; then
        update_tmux
elif [ "$files" == "git" ]; then
        update_git
fi
